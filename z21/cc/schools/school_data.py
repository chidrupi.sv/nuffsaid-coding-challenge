import time
import csv


def time_it(func):
    def decorator(self, *args, **kwargs):
        start = time.time()
        func_ret = func(self, *args, **kwargs)
        print(
            f'Time taken to execute the function {func.__name__} is {time.time() - start}')
        return func_ret
    return decorator


class SchoolData():
    """
    Base class responsible to fetch the data from csv and save it in memory
    """

    def __init__(self, input_file=''):
        import os
        cwd = os.getcwd()
        input_file = os.path.join(cwd, 'school_data.csv')
        with open(input_file) as inflie:
            reader = csv.DictReader(inflie)
            self.school_data = tuple(reader)
