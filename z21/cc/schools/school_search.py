from .school_data import SchoolData, time_it
from .constants import *
import itertools


class SchoolSearch(SchoolData):
    """
    This class is responsible for providing search capability for school data
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.school_name_lookup = {}
        self.city_name_lookup = {}
        self.school_id_lookup = {}

        # pre-process the data for easy search
        self._preprocess_data()
        self.search_list = []

    @time_it
    def _preprocess_data(self):
        """
        This function preprocess the data to have index on the search strings for easy access

        We maintain three dictionaries:
        School name lookup with each word in school name as key and list of schools as value
        City name lookup with each word in city name as key and list of schools as value
        ID lookup
        """
        for school in self.school_data:
            for word in school[NAME_COLUMN].split(' '):
                if word in COMMON_WORDS:
                    continue
                if word in self.school_name_lookup:
                    self.school_name_lookup[word].append(school)
                else:
                    self.school_name_lookup[word] = [school]
            for word in school[CITY_COLUMN].split(' '):
                if word in self.city_name_lookup:
                    self.city_name_lookup[word].append(school)
                else:
                    self.city_name_lookup[word] = [school]
            self.school_id_lookup[school[ID_COLUMN]] = school

    def _filter_data(self, school):
        """
        Takes the school and returns True if score is greater than 0
        """
        score = 0
        for item in self.search_list:
            if item in COMMON_WORDS:
                score += COMMON_MATCH
                continue
            if item in school[NAME_COLUMN]:
                score += NAME_MATCH
            if item in school[CITY_COLUMN]:
                score += CITY_MATCH
        school['score'] = score
        return not score

    def _search_complete_data(self) -> dict:
        """
        This function will search through the complete data as we didn't find anything in lookup
        """
        school_results = []
        for school in itertools.filterfalse(self._filter_data, self.school_data):
            school_results.append(school)

        school_results = sorted(
            school_results, key=lambda x: x['score'], reverse=True)
        return school_results[:3]

    def _sort_get_top(self, input_dict: dict, top: int) -> dict:
        """
        sorts the given dictionary with value (expecting it to be an integer)
        and return the top results in descending order
        """
        sorted_input = {k: v for k, v in sorted(
            input_dict.items(), key=lambda item: item[1], reverse=True)}
        top_results = {
            k: v for (k, v) in [item for item in sorted_input.items()][:top]}
        return top_results

    @time_it
    def search(self, name):
        """
        Performs the search operation against the data and returns top 3 relavant results
        """
        self.search_list = name.upper().split(' ')
        # We shall get common_match from initial result to add points at the end as we are not
        # including common words in lookup
        common_match = set(self.search_list) & set(COMMON_WORDS)
        school_match = {}
        # For all the items in searchlist find the school and add score
        # If the value is in school name it gets more score
        # If value is in city and not in school that gets more score
        for item in self.search_list:
            for school in self.school_name_lookup.get(item, []):
                try:
                    school_match[school[ID_COLUMN]] += NAME_MATCH
                except KeyError:
                    school_match[school[ID_COLUMN]] = NAME_MATCH

            for school in self.city_name_lookup.get(item, []):
                try:
                    school_match[school[ID_COLUMN]
                                 ] += CITY_MATCH if item not in school[NAME_COLUMN] else NAME_CITY_MATCH
                except KeyError:
                    school_match[school[ID_COLUMN]
                                 ] = CITY_MATCH if item not in school[NAME_COLUMN] else NAME_CITY_MATCH

        # We didn't get any match in index so we have to loop the complete data
        if not school_match:
            return self._search_complete_data()

        top_10 = self._sort_get_top(school_match, 10)
        for word in common_match:
            for item, score in top_10.items():
                if word in self.school_id_lookup[item][NAME_COLUMN]:
                    top_10[item] += NAME_MATCH
        top_3 = self._sort_get_top(top_10, 3)
        schools_top3 = [self.school_id_lookup[k]
                        for k in top_3.keys() if k in self.school_id_lookup]
        return schools_top3
