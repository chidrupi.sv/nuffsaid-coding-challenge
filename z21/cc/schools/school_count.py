from .school_data import SchoolData, time_it

import itertools


class SchoolCount(SchoolData):
    """
    This class is responsible to count the elements in school
    Initialize the class with csv, by default it uses the sample csv file in folder

    How counter works?

    Examples::
            >>> school_counter = SchoolCount()
            >>> school_counter.print_counts()
            >>> school_counter.LEANM05.count #you can give any column to find the count
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cur_item = ''

    def _sort_groupby_counter(self, iterable, key=None) -> itertools.groupby:
        """
        Sorts the given iterable with the key provided and also does a groupby
        Returns the itertools.groupby object
        """
        return itertools.groupby(sorted(iterable, key=key), key=key)

    def _groupby_counter(self, iterable, max_value=False):
        """
        Counts the values in the given itertools.groupby object 
        If max_value is True, it will also calculate the max count object

        Returns list(tuple(key, count)) if max_value is False
        Returns list(tuple(key, count)), (key, max_count ) if max_value is True
        """
        max_count = ('', 0)
        counter = []
        for key, data_it in iterable:
            data_ls = list(data_it)
            count_data = len(data_ls)
            counter.append((key, count_data))
            if max_value:
                max_count = (
                    key, count_data) if count_data > max_count[1] else max_count

        return (counter, max_count) if max_value else counter

    def format_result(self, name, result, tuple_names=()):
        """
        Formats the results
        """
        print('#'*20)
        print(name)
        print('#'*20)
        if isinstance(result, tuple):
            for ix, item in enumerate(result):
                print('-'*20)
                print(tuple_names[ix] if tuple_names else '')
                print('-'*20)
                print(item)
        else:
            print(result)

    @property
    @time_it
    def school_count(self):
        """
        returns total count in the school
        """
        return len(self.school_data)

    def __getattr__(self, item):
        if item not in self.school_data[0].keys():
            raise KeyError(f'Unknown attribute {item}')
        self.cur_item = item
        return self

    @property
    @time_it
    def counter(self):
        if not self.cur_item:
            raise 'Attribute call is missing. Please call school_count.LSTATE05.count'
        iterable = self._sort_groupby_counter(
            self.school_data, lambda x: x[self.cur_item])
        result = self._groupby_counter(iterable)
        self.cur_item = ''
        return result

    @property
    @time_it
    def count_max(self):
        if not self.cur_item:
            raise 'Attribute call is missing. Please call school_count.LSTATE05.count'
        iterable = self._sort_groupby_counter(
            self.school_data, lambda x: x[self.cur_item])
        result = self._groupby_counter(iterable, max_value=True)
        self.cur_item = ''
        return result

    def print_counts(self):
        self.format_result('SCHOOL COUNT', self.school_count)
        self.format_result('Schools by State:', self.LSTATE05.counter)
        self.format_result(
            'Schools by Metro-centric locale:', self.MLOCALE.counter)
        result = self.LCITY05.count_max
        self.format_result('City count', (result[1], len(
            result[0])), ('City with most schools:', 'Unique with at least one school:'))
