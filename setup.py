"""A setuptools based setup module.
See:
https://packaging.python.org/en/latest/distributing.html
"""
import setuptools
from z21.cc.schools import __version__


setup_args = {
    "name": "z21.cc.schools",
    "version": __version__,
    "description": "School search",
    "url": "",
    "author": "Chidrupi Sista",
    "classifiers": [
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Database",
        "License :: Other/Proprietary License",
        "Programming Language :: Python :: 3.6",
    ],
    # beware adding *.json file as package data, make sure not to add
    # param_patches.json file
    # "package_dir": {"": "."},
    # "packages": setuptools.find_packages(where=""),
    # "setup_requires": ["wheel", "pytest-runner"],
}


setuptools.setup(**setup_args)
