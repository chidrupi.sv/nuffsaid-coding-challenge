from z21.cc.schools.school_count import SchoolCount
from z21.cc.schools.school_search import SchoolSearch

if __name__ == "__main__":
    school_data = SchoolCount()
    school_data.print_counts()

    school_search = SchoolSearch()
    print(school_search.search('elementary school highland park'))
    print(school_search.search('jefferson belleville'))
    print(school_search.search('riverside school 44'))
    print(school_search.search('granada charter school'))
    print(school_search.search('KUSKOKWIM'))
